/**
 * 
 */
package com.example.vendor.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import lombok.Data;

/**
 * @author hemanth.garlapati
 * 
 */
@Configuration
@ConfigurationProperties
@PropertySource("classpath:messages_en.properties")
@Data
public class ConfigurationManager {
	
	private String itemNotFound;
	private String vendorNotFound;

}
