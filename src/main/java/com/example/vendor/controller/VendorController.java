/**
 * 
 */
package com.example.vendor.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.vendor.dto.ItemVendorDto;
import com.example.vendor.service.ItemService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author hemanth.garlapati
 * 
 */
@RestController
public class VendorController {
	Logger logger = LoggerFactory.getLogger(VendorController.class);
	
	@Autowired
	ItemService itemService;
	
	/**
	 * This method is getting the list of items available along with vendors
	 * 
	 * @param itemName
	 * @return ItemVendorDto
	 */
	@ApiOperation(value = "Retrieving items")
	@ApiResponses(value = {@ApiResponse(code = 4043, message = "no item found"),
			@ApiResponse(code = 4044, message = "no vendor found")
			 })
	@PostMapping("/items/{itemName}")
	public ResponseEntity<ItemVendorDto> fetchItems(@PathVariable String itemName)  {
		logger.info("VendorController : fetchItems");
		return new ResponseEntity<>(itemService.getItems(itemName), HttpStatus.OK);
	}

}
