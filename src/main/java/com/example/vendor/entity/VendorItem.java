/**
 * 
 */
package com.example.vendor.entity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author hemanth.garlapati
 * 
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class VendorItem {

	@EmbeddedId
	VendorItemPk vendorItemPk;

	private Double price;
	private Boolean availableStatus;

}
