/**
 * 
 */
package com.example.vendor.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author hemanth.garlapati
 * 
 */
@Embeddable
@Data
@AllArgsConstructor
@NoArgsConstructor
public class VendorItemPk implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Column(name = "vendor_id")
	private Long vendorId;
	
	@Column(name = "item_id")
	private Long itemId;
}
