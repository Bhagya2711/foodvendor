/**
 * 
 */
package com.example.vendor.dto;

import java.util.List;

import com.example.vendor.entity.Vendor;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author hemanth.garlapati
 * 
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ItemVendorDto {
	private Long itemId;
	private String itemName;
	private String description;
	private List<Vendor> vendorList;

}
