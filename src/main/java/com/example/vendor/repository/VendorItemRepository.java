/**
 * 
 */
package com.example.vendor.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.vendor.entity.VendorItem;

/**
 * @author hemanth.garlapati
 * 
 */
@Repository
public interface VendorItemRepository  extends JpaRepository<VendorItem, Long>{
	
	@Query(value = "select vendor_id from vendor_item v where v.item_id=?1", nativeQuery = true)
	List<Long> findByItemId(Long itemId);

}
