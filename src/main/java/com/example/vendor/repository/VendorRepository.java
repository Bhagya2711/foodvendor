/**
 * 
 */
package com.example.vendor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.vendor.entity.Vendor;

/**
 * @author hemanth.garlapati
 * 
 */
@Repository
public interface VendorRepository extends JpaRepository<Vendor, Long>{

}
