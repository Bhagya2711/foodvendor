/**
 * 
 */
package com.example.vendor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.vendor.entity.Item;

/**
 * @author hemanth.garlapati
 * 
 */
@Repository
public interface ItemRepository extends JpaRepository<Item, Long> {
	Item findByItemName(String itemName);

}
