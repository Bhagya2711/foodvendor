/**
 * 
 */
package com.example.vendor.enums;

/**
 * @author hemanth.garlapati
 *
 */
public enum ErrorCodes {
	VALIDATION_FAILED(4001l),NO_ITEM_FOUND(4043l),VENDOR_NOT_FOUND(4044l);

	Long errorCode;

	ErrorCodes(Long value) {
		this.errorCode = value;
	}

	public Long getErrorCode() {
		return errorCode;
	}

}
