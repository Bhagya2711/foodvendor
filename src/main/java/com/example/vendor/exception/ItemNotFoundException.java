/**
 * 
 */
package com.example.vendor.exception;

/**
 * @author hemanth.garlapati
 * 
 */
public class ItemNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ItemNotFoundException(String arg0) {
		super(arg0);

	}

}
