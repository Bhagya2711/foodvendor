/**
 * 
 */
package com.example.vendor.exception;

/**
 * @author hemanth.garlapati
 * 
 */
public class VendorNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public VendorNotFoundException(String arg0) {
		super(arg0);

	}

}
