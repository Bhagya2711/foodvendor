/**
 * 
 */
package com.example.vendor.exception;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author hemanth.garlapati
 * 
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ErrorResponse {
    Long errorCode;
    List<String> errorMessage;
    

 

}