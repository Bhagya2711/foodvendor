/**
 * 
 */
package com.example.vendor.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.vendor.config.ConfigurationManager;
import com.example.vendor.dto.ItemVendorDto;
import com.example.vendor.entity.Item;
import com.example.vendor.entity.Vendor;
import com.example.vendor.exception.ItemNotFoundException;
import com.example.vendor.exception.VendorNotFoundException;
import com.example.vendor.repository.ItemRepository;
import com.example.vendor.repository.VendorItemRepository;
import com.example.vendor.repository.VendorRepository;

/**
 * @author hemanth.garlapati
 * 
 */
@Service
public class ItemServiceImpl implements ItemService {

	Logger logger = LoggerFactory.getLogger(ItemServiceImpl.class);

	@Autowired
	ItemRepository itemRepository;

	@Autowired
	ConfigurationManager configurationManager;

	@Autowired
	VendorItemRepository vendorItemRepository;

	@Autowired
	VendorRepository vendorRepository;

	public ItemVendorDto getItems(String itemName) {

		Item item = itemRepository.findByItemName(itemName);

		if (item == null) {
			throw new ItemNotFoundException(configurationManager.getItemNotFound());
		}
		Long itemId = item.getItemId();

		List<Long> vendorIdList = vendorItemRepository.findByItemId(itemId);

		List<Vendor> vendors = new ArrayList<>();
		vendorIdList.forEach(vendorId -> {
			Optional<Vendor> optionalVendor = vendorRepository.findById(vendorId);
			if (!optionalVendor.isPresent()) {
				throw new VendorNotFoundException(configurationManager.getVendorNotFound());
			}
			Vendor vendor = new Vendor();
			vendor.setVendorId(optionalVendor.get().getVendorId());
			vendor.setVendorName(optionalVendor.get().getVendorName());
			;
			vendors.add(vendor);

		});

		ItemVendorDto itemVendorDto = new ItemVendorDto();
		itemVendorDto.setItemId(itemId);
		itemVendorDto.setItemName(itemName);
		itemVendorDto.setDescription(item.getDescription());
		itemVendorDto.setVendorList(vendors);

		return itemVendorDto;

	}

}
