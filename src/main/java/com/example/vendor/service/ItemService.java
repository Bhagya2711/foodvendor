/**
 * 
 */
package com.example.vendor.service;

import org.springframework.stereotype.Service;

import com.example.vendor.dto.ItemVendorDto;

/**
 * @author hemanth.garlapati
 * 
 */
@Service
public interface ItemService {
	public ItemVendorDto getItems(String itemName);

}
