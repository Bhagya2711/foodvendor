/**
 * 
 */
package com.example.vendor.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.example.vendor.dto.ItemVendorDto;
import com.example.vendor.entity.Vendor;
import com.example.vendor.service.ItemServiceImpl;

/**
 * @author hemanth.garlapati
 * 
 */
@ExtendWith(MockitoExtension.class)
public class VendorControllerTest {

	@Mock
	ItemServiceImpl itemServiceImpl;

	@InjectMocks
	VendorController vendorController;

	@Test
	public void testfetchItems() {

		ItemVendorDto itemVendorDto = new ItemVendorDto();
		itemVendorDto.setItemId(1L);
		itemVendorDto.setItemName("biryani");
		itemVendorDto.setDescription("yummy");
		Vendor vendor = new Vendor(1L, "Annapoorna");
		List<Vendor> vendors = new ArrayList<>();
		vendors.add(vendor);
		itemVendorDto.setVendorList(vendors);
		Mockito.when(itemServiceImpl.getItems("biryani")).thenReturn(itemVendorDto);
		assertEquals(200, vendorController.fetchItems("biryani").getStatusCodeValue());
	}

}
