/**
 * 
 */
package com.example.vendor.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.example.vendor.config.ConfigurationManager;
import com.example.vendor.entity.Item;
import com.example.vendor.entity.Vendor;
import com.example.vendor.exception.ItemNotFoundException;
import com.example.vendor.exception.VendorNotFoundException;
import com.example.vendor.repository.ItemRepository;
import com.example.vendor.repository.VendorItemRepository;
import com.example.vendor.repository.VendorRepository;

/**
 * @author hemanth.garlapati
 * 
 */
@ExtendWith(MockitoExtension.class)
public class ItemServiceImplTest {

	@InjectMocks
	ItemServiceImpl itemServiceImpl;

	@Mock
	ItemRepository itemRepository;

	@Mock
	VendorRepository vendorRepository;

	@Mock
	VendorItemRepository vendorItemRepository;

	@Mock
	ConfigurationManager configurationManager;

	@BeforeEach
	public void init() {

	}

	@Test
	public void testgetItems() {
		Item item = new Item(1L, "biryani", "yummy");
		Mockito.when(itemRepository.findByItemName("biryani")).thenReturn(item);
		List<Long> list = new ArrayList<>();
		list.add(1L);
		Mockito.when(vendorItemRepository.findByItemId(item.getItemId())).thenReturn(list);
		Vendor vendor = new Vendor(1L, "Annapoorna");
		Mockito.when(vendorRepository.findById(1L)).thenReturn(Optional.of(vendor));
		assertEquals(1L, itemServiceImpl.getItems("biryani").getItemId());

	}

	@Test
	public void testgetItemsForItemNotFoundException() {

		assertThrows(ItemNotFoundException.class, () -> itemServiceImpl.getItems("biryano"));

	}
	
	@Test
	public void testgetItemsForVendorNotFoundException() {
		Item item = new Item(1L, "biryani", "yummy");
		Mockito.when(itemRepository.findByItemName("biryani")).thenReturn(item);
		List<Long> list = new ArrayList<>();
		list.add(2L);
		Mockito.when(vendorItemRepository.findByItemId(item.getItemId())).thenReturn(list);
		//Vendor vendor = new Vendor(1L, "Annapoorna");
		//Mockito.when(vendorRepository.findById(1L)).thenReturn(Optional.of(vendor));
		assertThrows(VendorNotFoundException.class,() ->itemServiceImpl.getItems("biryani"));
	}

}
